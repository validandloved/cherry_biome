--[[
    Copyright (C) 2023
Atlante
AtlanteWork@gmail.com

                      GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.
5349666272645780583
]]


if minetest.get_modpath("logspikes") ~= nil then
	logspikes.register_log_spike('cherry_biome:cherry_biome_sakura_spike',          'cherry_biome:sakura_tree');
end